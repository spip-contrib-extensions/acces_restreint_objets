<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/profils.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// P
	'accesrestreintobjets_description' => 'Le plugin Accès restreint ne permet que de restreindre des rubriques (et en cascade les contenus dedans). Ce plugin ajoute une interface permettant de mettre dans une zone restreinte n’importe quel contenu (événements, articles, patates…), y compris donc des articles à l’unité, qui seraient dans des rubriques non restreintes. Chacun de ces contenus individuels est alors restreint suivant les règles des zones attachées.',
	'accesrestreintobjets_nom' => 'Accès restreint sur tous les contenus',
);
